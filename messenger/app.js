const 
  bodyParser = require('body-parser'),
  crypto = require('crypto'),
  express = require('express'),
  https = require('https'),
  request = require('request');

const app = express()
const APP_SECRET = '6bb584394b4704ec438b2a85c99607ef'
const VALIDATION_TOKEN = 'SilahkanDiisiApaunTokenRandomKalian'
const PAGE_ACCESS_TOKEN = 'EAAVx5JTRdZA8BALZAtOX14IoYgsVYrO7W4DyLoNRiXZBmnLvDv3xChQsiWShN6lVvQZC1uQApNfB6SdASVVs3xvkyqhZAzEBJSZAbceS6Si5RUVkmZBxUjeZAfFsX7GjwKNrvZBXDdajQTgjOiYJKrStSbOOwm6RQ4PXIA5GEBp2bNwZDZD'
const IMG_BASE_PATH = 'https://campaignday.com/static/'

app.set('port', 8080)
app.use(bodyParser.json({ verify: verifyRequestSignature }))

function verifyRequestSignature(req, res, buf) {
  var signature = req.headers["x-hub-signature"];

  if (!signature) {
    // In DEV, log an error. In PROD, throw an error.
    console.error("Couldn't validate the signature.");
  } else {
    var elements = signature.split('=');
    var method = elements[0];
    var signatureHash = elements[1];

    var expectedHash = crypto.createHmac('sha1', APP_SECRET)
                        .update(buf)
                        .digest('hex');

    console.log("received  %s", signatureHash);
    console.log("exepected %s", expectedHash);
    if (signatureHash != expectedHash) {
      throw new Error("Couldn't validate the request signature.");
    }
  }
}

app.get('/webhook/', (req, res) => {
  if (req.query['hub.verify_token'] === VALIDATION_TOKEN) {
    res.send(req.query['hub.challenge'])
  } else {
    res.send('Error, wrong validation token')
  }
})


app.post('/webhook', (req, res) => {
  console.log('message received!')
  let data = req.body 
  console.log(JSON.stringify(data))

  if (data.object == 'page') {
    res.sendStatus(200)
    // entries from multiple pages may be batched in one request
    data.entry.forEach(pageEntry => {
      // iterate over each messaging event for this page
      pageEntry.messaging.forEach(messagingEvent => {
        let propertyNames = []
        for (let prop in messagingEvent) {
          propertyNames.push(prop)
        }
        console.log("[app.post] Webhook event props ", propertyNames.join())
        if (messagingEvent.message) {
          processMesssageFromPage(messagingEvent)
        } else if (messagingEvent.postback) {
          processPostbackMessage(messagingEvent)
        } else {
          console.log("[app.post] not prepared to handle this message type.");
        }
      })
    })
  }
})

function processMesssageFromPage(event) {
  var senderID = event.sender.id 
  var pageID = event.recipient.id 
  var timeOfMessage = event.timestamp
  var message = event.message

  console.log("[processMessageFromPage] user (%d) page (%d) timestamp (%d) and message (%s)", 
    senderID, pageID, timeOfMessage, JSON.stringify(message))
  
  if (message.quick_reply) {
    console.log("[processMessageFromPage] quick_reply.payload (%s)", message.quick_reply.payload)
    handleQuickReplyResponse(event)
    return
  }

  var messageText = message.text 
  if (messageText) {
    console.log("[processMessageFromPage] (%s)", messageText);
    var lowerCaseMsg = messageText.toLowerCase()
    switch(lowerCaseMsg) {
      case 'help': 
        sendHelpOptionsAsQuickReplies(senderID)
        break
      default:
        sendTextMessage(senderID, messageText)
    }
    
  }
}

function sendTextMessage(recipientID, messageText) {
  var messageData = {
    recipient: {
      id: recipientID
    },
    message: {
      text: messageText
    }
  }
  console.log("[sentTextMessage] %s", JSON.stringify(messageData))
  callSendAPI(messageData)
}

function processPostbackMessage(event) {
  var senderID = event.sender.id
  var recipientID = event.recipient.id 
  var timeOfPostBack = event.timestamp

  var payload = event.postback.payload

  console.log("[processPostBackMessage] from user %d on page %d with payload %s at %d", senderID, recipientID, payload, timeOfPostBack)
}

function sendHelpOptionsAsQuickReplies(recipientID) {
  console.log("[send help quick reply] Sending help menu")
  var messageData = {
    recipient: { id: recipientID },
    message: {
      text: "Select a feature to learn more.",
      quick_replies: [
        { "content_type": "text", "title": "Ticket", "payload": "HELP_1" },
        { "content_type": "text", "title": "Event", "payload": "HELP_2" },
        { "content_type": "text", "title": "News", "payload": "HELP_3" },
      ]
    }
  }

  callSendAPI(messageData)
}

function handleQuickReplyResponse(event) {
  var senderID = event.sender.id
  var pageID = event.recipient.id
  var message = event.message
  var quickReplyPayload = message.quick_reply.payload

  console.log("[handleQuickReplyResponse] Handling quick reply response (%s) from sender (%d) to page (%d) with message (%s)", 
  quickReplyPayload, senderID, pageID, JSON.stringify(message))

  responsdToHelpRequestWithTemplates(senderID, quickReplyPayload)
  
}

function responsdToHelpRequestWithTemplates(senderID, payload) {
  var templateElements = []
  var sectionButtons = []
  var addSectionButton = function(title, payload) {
    sectionButtons.push({
      type: 'postback',
      title: title,
      payload: payload
    });
  }
  
  switch (payload) {
    case 'HELP_1':
      addSectionButton('Photo', 'QR_PHOTO_1')
      addSectionButton('Caption', 'QR_CAPTION_1')
      addSectionButton('Background', 'QR_BACKGROUND_1')

      templateElements.push(
        {
          title: "Rotation",
          subtitle: "portrait mode",
          image_url: IMG_BASE_PATH + "01-rotate-landscape.png",
          buttons: sectionButtons 
        }, 
        {
          title: "Rotation",
          subtitle: "landscape mode",
          image_url: IMG_BASE_PATH + "02-rotate-portrait.png",
          buttons: sectionButtons 
        }
      )

      console.log('HELP_1')

      break
    case 'HELP_2':
      break
    case 'HELP_3':
      break
    default:
  }

  var messageData = {
    recipient: { id: senderID },
    message: {
      attachment: {
        type: 'template',
        payload: {
          template_type: 'generic',
          elements: templateElements
        }
      }
    }
  }

  callSendAPI(messageData)

}

/*
 * Call the Send API. If the call succeeds, the message id is returned in the response.
 */
function callSendAPI(messageData) {
  request({
    uri: 'https://graph.facebook.com/v2.6/me/messages',
    qs: { access_token: PAGE_ACCESS_TOKEN },
    method: 'POST',
    json: messageData

  }, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var recipientId = body.recipient_id;
      var messageId = body.message_id;

      if (messageId) {
        console.log("[callSendAPI] message id %s sent to recipient %s", 
          messageId, recipientId);
      } else {
        console.log("[callSendAPI] called Send API for recipient %s", 
          recipientId);
      }
    } else {
      console.error("[callSendAPI] Send API call failed", response.statusCode, response.statusMessage, body.error);
    }
  });  
}

/**
 * Start Server
 */
app.listen(app.get('port'), () => {
  console.log('Node app running')
})

module.exports = app