# React & React Native Course


## Requirement Software

1. Web Browser, kami rekomendasikan Google Chrome
2. NodeJs https://nodejs.org/dist/v6.11.5/node-v6.11.5-x64.msi, Nodejs versi 8 saat ini belum di support.
3. Android Studio IDE, untuk melakukan instalasi bisa melihat di https://developer.android.com/studio/install.html
4. Visual Studio Code
5. GenyMotion with VirtualBox (opsional jika ingin melakukan preview react native di simulator)

Semua file di atas bisa di download di Google Drive ini https://goo.gl/Sg7S67

## Persiapan Lab

### 1. ReactJs 

Tidak ada persiapan khusus.

### 2. React Native

Setelah selesai melakukan instalasi android studio lakukan langkah-langkah berikut ini:

1. Register platform tools android SDK
    1. Di  Windows 7/8:
        - dari desktop, klik kanan My Computer dan pilih Properties
        - di System Properties, klik Advanced tab
        - di Advanced section, klik tombol Environment Variables
        - di the Environment Variables, highlight Path variable di bagian Systems Variable section dan klik tombol Edit
        - tambahkan ```C:\Users\<user>\AppData\Local\Android\Sdk\platform-tools di akhir baris```
    
    2. Di Windows 10:
        - buka Start menu, dan ketik “advanced system settings”
        - pilih “View advanced system settings”
        - klik Advanced tab
        - buka “Environment Variables” window
        - pilih the Path variable di bawah “System Variables" dan klik tombol “Edit”
        - klik tombol “Edit Text”
        - tambahkan  ```C:\Users\<user>\AppData\Local\Android\Sdk\platform-tools```
    
    3. Di Mac, silakan ikuti petunjuk di [https://developer.android.com/studio/install.html](https://developer.android.com/studio/install.html)
        
2. Install react native cli ```npm install -g create-react-native-app```
3. Install expo ```npm install -g exp``` dan buatlah akun expo di https://expo.io
4. Optional, install expo client di mobile untuk melakukan live preview di device yang sesungguhnya
    - untuk Android https://play.google.com/store/apps/details?id=host.exp.exponent&referrer=www
    - untuk IOS https://itunes.apple.com/app/apple-store/id982107779?ct=www&mt=8


### 3. Facebook Analytics

1. Kunjungi https://developers.facebook.com/
2. Signin dan buatlah satu buah apps untuk melakukan testing


## Pertanyaan

Apabila anda kesulitan dalam langkah-langkah di atas, silahkan hubungi saya di purwandi@froyo.co.id atau didit@froyo.co.id

## Tentang Froyo Framework

Froyo Framework adalah mitra belajar dalam memahami teknologi terkini. Kami dapat membantu Anda
dalam menempuh jalur menjadi seorang developer yang terampil, karena Kami memiliki misi untuk membantu
mengembangkan Indonesia melalui teknologinya.

Kurikulum kami dibuat sedemikian rupa untuk membantu proses belajar Anda dengan nyaman,
menggunakan pemahaman teknologi terkini yang dibutuhkan oleh industri IT. 

Informasi lebih lanjut, silakan kunjungi [www.framework.id](http://www.framework.id).



## License

Materi pelatihan ini adalah open source dan berlisensi MIT