const bodyParser = require('body-parser')
const express = require('express')
const app = express()
const Request  = require('request');
const Querystring  = require('querystring');

const FACEBOOK_APP_ID = '1532601446790559'
const FACEBOOK_APP_AK_SECRET = '93b5e7f7766632433f290581dba49a53'
const FACEBOOK_APP_AK_VERSION = 'v1.1'
const FACEBOOK_API_ME = 'https://graph.accountkit.com/' + FACEBOOK_APP_AK_VERSION + '/me';
const FACEBOOK_API_ACCESS = 'https://graph.accountkit.com/' + FACEBOOK_APP_AK_VERSION + '/access_token'; 

app.use(bodyParser.urlencoded({ extended: false}))
app.use(bodyParser.json());
app.use(express.static('public'))
app.set('view engine', 'pug')

app.get('/', (req, res) => {
  res.render('index')
})

app.get('/account', (req, res) => {
  var app_access_token = 'AA' +'|'+ FACEBOOK_APP_ID +'|'+FACEBOOK_APP_AK_SECRET
  var params = {
    grant_type: 'authorization_code',
    code: req.query.code,
    access_token: app_access_token
  }
  // exchange tokens
  var token_exchange_url = FACEBOOK_API_ACCESS + '?' + Querystring.stringify(params);
  Request.get({url: token_exchange_url, json: true}, (err, resp, respBody) =>  {
    var viewParams = {
      access_token: respBody.access_token,
      expires_at: respBody.expires_at,
      user_id: respBody.id,
      phone: '',
      email: ''
    };

    // get account details at /me endpoint
    var me_endpoint_url = FACEBOOK_API_ME + '?access_token=' + respBody.access_token;
    Request.get({url: me_endpoint_url, json:true }, (err, resp, respBody) => {
      // send login_success.html
      if (respBody.phone) {
        viewParams.phone = respBody.phone.number;
      } else if (respBody.email) {
        viewParams.email = respBody.email.address;
      }
      res.render('account', viewParams)
    });
  })
})

app.listen(3000, () => {
  console.log('App running ...')
})