# React JS Lab

Tulislah code berikut ini

```
import React from 'react'
export default class Products extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
        products: [
            { id: 1, name: 'Red shoes', price: 2000 },
            { id: 2, name: 'Blue shoes', price: 3000 },
            { id: 3, name: 'Black shoes', price: 4000 }
        ]
    }
  }

  render () {
    return (
      <div className="row">
        <div className="col-md-8">
          <ol>
            {this.state.products.map((product, index) => {
              return (
                <li key={index}>
                  {product.name} {product.price}
                  <button>Add to cart</button>
                </li>        
              )
            })}
          </ol>
        </div>
        <div className="col-md-4">
        </div>
      </div>
    );
  }
}
```

Mofifikasi App.js file untuk menampilkan product komponent

```
import React, { Component } from 'react';
import ProductsCarts from './components/products'

class App extends Component {
  render() {
    return (
      <div className="container">
        <header className="App-header">
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <ProductsCarts />

      </div>
    );
  }
}
export default App;
```

Modifikasi list product dengan menambahkan event click dan tambahkan handler addToCart

```
  addToCart(product) {
    console.log(product)
  }

  render() {
    <div className="row">
      <div className="col-md-8">
        <ol>
          {this.state.products.map((product, index) => {
            return (
              <li key={index}>
                {product.name} {product.price}
                <button onClick={this.addToCart.bind(this, product)}>Add to cart</button>
              </li>        
            )
          })}
        </ol>
      </div>
      <div className="col-md-4">
      </div>
    </div>
  }
```

Modifikasi state untuk menampung cart serta modifikasi event handler untuk logic cart

```
  constructor (props) {
    super(props)

    this.state = {
      products: [
          { id: 1, name: 'Red shoes', price: 2000 },
          { id: 2, name: 'Blue shoes', price: 3000 },
          { id: 3, name: 'Black shoes', price: 4000 }
      ],
      carts: []
    }  
  }

  addToCart(product) {
    let check = this.state.carts.find( p => p.id === product.id)
    if (!check) {
      let carts = {
          id: product.id,
          name: product.name,
          price: product.price,
          quantity: 1
      }
      this.setState(state => ({
          carts: [...state.carts, carts]
      }))
    } else {
      let carts = this.state.carts.map((item, index) => {
          if (item.id !== product.id) {
              return item
          }
          return {
            ...item,
            ...item.quantity++
          }
      })

      this.setState({ carts: carts })
    }
  }
```

Buat cart component untuk menampilkan hasil cart, hapus cart item dan melakukan kalkulasi total cart

```
class Carts extends React.Component {
   render () {
    return (
      <div>
        <h2>Carts</h2>
        <table className="table">
          <tbody>
            
          </tbody>
        </table>
      </div>
    )
  }
}

export default Carts
```

Modifikasi file product komponen untuk menampilkan cart komponen

```
import CartProducts from '../components/carts'


  render() {
    <div className="row">
      <div className="col-md-8">
        <ol>
          {this.state.products.map((product, index) => {
            return (
              <li key={index}>
                {product.name} {product.price}
                <button onClick={this.addToCart.bind(this, product)}>Add to cart</button>
              </li>        
            )
          })}
        </ol>
      </div>
      <div className="col-md-4">
        <CartProducts>
      </div>
    </div>
  }
```

Tambahkan props di dalam cart komponen untuk mendapatkan state cart dari produk komponen

```
class Carts extends React.Component {
   render () {
    return (
      <div>
        <h2>Carts</h2>
        <table className="table">
          <tbody>
            {this.props.carts.map((product, index) => {
              return (
                <tr key={index}>
                  <td>{product.name}</td>
                  <td>{product.quantity}</td>
                  <td>{product.price * product.quantity}</td>
                  <td><button>Remove</button></td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    )
  }
}

Carts.propTypes = {
  carts: PropTypes.array
}
```

```
Products ....
  render () {
    return (
      <Carts carts={this.state.carts} />
    )
  }

```

Pembuatan kalkulasi total

```
  totalPrice () {
    return this.props.carts.reduce((total, product) => {
      return total + product.price * product.quantity
    }, 0)
  }

  render() {
    return (
      <div>
        <h2>Carts</h2>
        <table className="table">
          <tbody>
            {this.props.carts.map((product, index) => {
              return (
                <tr key={index}>
                  <td>{product.name}</td>
                  <td>{product.quantity}</td>
                  <td>{product.price * product.quantity}</td>
                  <td><button>Remove</button></td>
                </tr>
              )
            })}
            <tr>
              <td></td>
              <td>Total</td>
              <td>{this.totalPrice()}</td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
```

Buat menhapus cart

```
class Product ... 

  constructor () {
    // ...
     // Bind the this context to the handler function
    this.removeFromCart = this.removeFromCart.bind(this)
    // ... 
  }
  removeFromCart (product, index) {
    let carts = this.state.carts
    carts.splice(index, 1)
    this.setState({ carts: carts })
  }
  render () {
    return (
      <Carts carts={this.state.carts} removeFromCart={this.removeFromCart} />
    )
  }
```

```
class Carts extends React.Component {
  render() {
    return (
      <td><button onClick={this.props.removeFromCart.bind(this, product, index)}>Remove</button></td>
    )
  }
}

Carts.propTypes = {
  carts: PropTypes.array,
  removeFromCart: PropTypes.func
}
```