import React, { Component } from 'react';
import ProductsCarts from './components/products'

class App extends Component {
  render() {
    return (
      <div className="container">
        <header className="App-header">
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <ProductsCarts />

      </div>
    );
  }
}

export default App;
