import React from 'react'
import PropTypes from 'prop-types'

class Carts extends React.Component {

  totalPrice () {
    return this.props.carts.reduce((total, product) => {
      return total + product.price * product.quantity
    }, 0)
  }

  render () {
    return (
      <div>
        <h2>Carts</h2>
        <table className="table">
          <tbody>
            {this.props.carts.map((product, index) => {
              return (
                <tr key={index}>
                  <td>{product.name}</td>
                  <td>{product.quantity}</td>
                  <td>{product.price * product.quantity}</td>
                  <td><button onClick={this.props.removeFromCart.bind(this, product, index)}>Remove</button></td>
                </tr>
              )
            })}
            <tr>
              <td></td>
              <td>Total</td>
              <td>{this.totalPrice()}</td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
}

Carts.propTypes = {
  carts: PropTypes.array,
  removeFromCart: PropTypes.func
}

export default Carts