import React from 'react'
import Carts from '../carts'

export default class Products extends React.Component {

  constructor (props) {
    super(props)

    // Bind the this context to the handler function
    this.removeFromCart = this.removeFromCart.bind(this)

    this.state = {
      products: [
          { id: 1, name: 'Red shoes', price: 2000 },
          { id: 2, name: 'Blue shoes', price: 3000 },
          { id: 3, name: 'Black shoes', price: 4000 }
      ],
      carts: []
    }  
  }

  addToCart (product) {
    let check = this.state.carts.find( p => p.id === product.id)
    if (!check) {
      let carts = {
          id: product.id,
          name: product.name,
          price: product.price,
          quantity: 1
      }
      this.setState(state => ({
          carts: [...state.carts, carts]
      }))
    } else {
      let carts = this.state.carts.map((item, index) => {
          if (item.id !== product.id) {
              return item
          }
          return {
            ...item,
            ...item.quantity++
          }
      })

      this.setState({ carts: carts })
    }
  }

  removeFromCart (product, index) {
    let carts = this.state.carts
    carts.splice(index, 1)
    this.setState({ carts: carts })
  }

  render () {
    return (
      <div className="row">
        <div className="col-md-8">
          <ol>
            {this.state.products.map((product, index) => {
              return (
                <li key={index}>
                  {product.name} {product.price}
                  <button onClick={this.addToCart.bind(this, product)}>Add to cart</button>
                </li>        
              )
            })}
          </ol>
        </div>
        <div className="col-md-4">
          <Carts carts={this.state.carts} removeFromCart={this.removeFromCart} />
        </div>
      </div>
    );
  }
} 