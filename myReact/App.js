import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import FacebookLogin, { getPictureUrl } from './components/facebook'

export default class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      url: 'http://via.placeholder.com/250x250'
    }
  }

  getProfilePicture () {
    const url = getPictureUrl()
    fetch(url).then(response => {
      const { url } = response
      this.setState({ url })
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>

        <TouchableOpacity onPress={FacebookLogin}>
          <Text style={{fontSize: 24}}>Login Facebook</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={this.getProfilePicture.bind(this)}>
          <Text style={{fontSize: 24}}>View Facebook Profile</Text>
        </TouchableOpacity>

        <Image style={{ width: 50, height: 50 }} source={{ uri: this.state.url }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
