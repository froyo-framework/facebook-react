import React from 'react'
import { Alert } from 'react-native'
import { Facebook } from 'expo'
import qs from 'qs'

let currentToken = ''

export default async function login () {
  try {
    const { type, token } = await Facebook.logInWithReadPermissionsAsync(
      '1532601446790559',
      { permissions: ['public_profile', 'email']}
    )
  
    currentToken = token
    if (type === 'success') {
      let params = {
        access_token: token,
        fields: 'id,name,email'
      }
      let url = 'https://graph.facebook.com/me?' + qs.stringify(params);
      const response = await fetch(url)
      return response.json()
    } else if (type === 'cancel') {
      Alert.alert('Canceled!!!', 'Login was canceled!')
    } else {
      Alert.alert('Opps!!!', 'Login failed!')
    }
  } catch (error) {
    Alert.alert('Opps!!!', 'Login failed!')
  }
  
}

export function getPictureUrl() {
  return 'https://graph.facebook.com/me/picture?access_token=' + currentToken
}